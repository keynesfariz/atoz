<?php
Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('order', 'HomeController@index')->name('home');
Route::get('product', 'ProductController@index');
Route::post('product', 'ProductController@store');
Route::get('prepaid-balance', 'PrepaidBalanceController@index');
Route::post('prepaid-balance', 'PrepaidBalanceController@store');
Route::get('payment', 'HomeController@showPayment');
Route::post('payment', 'HomeController@makePayment');