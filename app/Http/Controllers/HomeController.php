<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('order')) {

            if (strlen($request->order) < 10) {
                $orders = Order::with('orderable')
                    ->where([
                        ['user_id', '=', auth()->id()],
                        ['order_number', 'like', '%'.$request->order.'%']
                    ])
                    ->latest()
                    ->paginate(20);
            } else {
                $orders = Order::with('orderable')
                    ->where([
                        ['user_id', '=', auth()->id()],
                        ['order_number', '=', $request->order]
                    ])
                    ->latest()
                    ->get();
            }
        } else {

            $orders = Order::with('orderable')
                ->where('user_id', auth()->id())
                ->latest()
                ->paginate(20);
        }

        return view('order', compact('orders'));
    }

    public function showPayment(Request $request)
    {
        $order = null;
        if ($request->has('order')) {
            $order = $request->order;
        }

        return view('payment', compact('order'));
    }

    public function makePayment(Request $request)
    {
        $request->validate([
            'order_number' => 'required|exists:orders'
        ]);

        $order = Order::where('order_number', $request->order_number)->first();
        
        if ($order->orderable_type == 'App\Product') {
            $order->status = 2;
            $product = $order->orderable;
            $product->shipping_code = strtoupper(str_random(10));
            $product->save();
        } else {
            // if this is prepaid balance, probability success rate...
            $order->status = 2;
        }
        $order->save();

        return redirect('order');
    }
}
