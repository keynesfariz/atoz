<?php

namespace App\Http\Controllers;

use App\Order;
use App\PrepaidBalance;
use Illuminate\Http\Request;

class PrepaidBalanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	return view('prepaid');
    }

    public function store(Request $request)
    {
    	$request->validate([
    		'phone' => 'required|string|min:7|max:12|regex:/081\d{4,9}/',
    		'value' => 'required|numeric',
    	]);

    	$prepaid = new PrepaidBalance;
    	$prepaid->phone = $request->phone;
    	$prepaid->value = $request->value;
    	$prepaid->save();

        $faker = \Faker\Factory::create();

    	$order = new Order;
    	$order->user_id = auth()->id();
    	$order->order_number = $faker->numerify('##########');
    	$order->total = $request->value * 1.05;
    	$prepaid->orders()->save($order);

    	return view('thanks', compact('order'));

    }
}
