<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	return view('product');
    }

    public function store(Request $request)
    {
    	$request->validate([
    		'product' => 'required|string|min:10|max:150',
    		'address' => 'required|string|min:10|max:150',
    		'price' => 'required|numeric',
    	]);

    	$product = new Product;
    	$product->product = $request->product;
    	$product->address = $request->address;
    	$product->price = $request->price;
    	$product->save();

    	$faker = \Faker\Factory::create();

    	$order = new Order;
    	$order->user_id = auth()->id();
    	$order->order_number = $faker->numerify('##########');
    	$order->total = $request->price + 10000;
    	$product->orders()->save($order);

    	return view('thanks', compact('order'));
    }
}
