@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <h4 class="text-center">Your Order Number <br><strong>{{ $order->order_number }}</strong></h4><br>
          <h4 class="text-center">Total <br><strong>{{ number_format($order->total, 0, ',', '.') }}</strong></h4><br>

          @if ($order->orderable_type == 'App\PrepaidBalance')
          <h5 class="text-center">Your mobile phone number {{ $order->orderable->phone }} will be topped up <br>for {{ number_format($order->orderable->value, 0, ',', '.') }} after you pay</h5>
          @else
          <h5 class="text-center">{{ $order->orderable->product }} that cost {{ $order->orderable->price }} <br>will be shipped to {{ $order->orderable->address }} after you pay</h5>
          @endif

          <br>
          <a href="{{ url('payment?order='.$order->order_number) }}" class="btn btn-block btn-primary">Pay Here</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection