@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Top Up Prepaid Balance
        </div>
        <div class="card-body">
          <form method="post" action="{{ url('payment') }}">
            @csrf
            <div class="form-group row">
              <label for="order_number" class="col-sm-4 col-form-label text-md-right">Order Number</label>
              <div class="col-md-6">
                <input id="order_number" type="text" class="form-control {{ $errors->has('order_number') ? ' is-invalid' : '' }}" name="order_number" value="{{ $order or old('order_number') }}" required autofocus maxlength="10" minlength="10">
                @if ($errors->has('order_number'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('order_number') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group row mb-0">
              <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                  Pay
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection