@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Buy Products
        </div>
        <div class="card-body">
          <form method="post" action="{{ url('product') }}">
            @csrf
            <div class="form-group row">
              <label for="product" class="col-sm-4 col-form-label text-md-right">Product</label>
              <div class="col-md-6">
                <textarea rows="4" id="product" class="form-control {{ $errors->has('product') ? ' is-invalid' : '' }}" name="product" required autofocus>{{ old('product') }}</textarea>
                @if ($errors->has('product'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('product') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group row">
              <label for="address" class="col-sm-4 col-form-label text-md-right">Shipping Address</label>
              <div class="col-md-6">
                <textarea rows="4" id="address" class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" required autofocus>{{ old('address') }}</textarea>
                @if ($errors->has('address'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('address') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group row">
              <label for="price" class="col-sm-4 col-form-label text-md-right">Price</label>
              <div class="col-md-6">
                <input type="text" id="price" name="price" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" required value="{{ old('price') }}">
                @if ($errors->has('price'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('price') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group row mb-0">
              <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                  Submit
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection 