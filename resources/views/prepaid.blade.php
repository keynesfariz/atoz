@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          Top Up Prepaid Balance
        </div>
        <div class="card-body">
          <form method="post" action="{{ url('prepaid-balance') }}">
            @csrf
            <div class="form-group row">
              <label for="phone" class="col-sm-4 col-form-label text-md-right">Mobile Phone Number</label>
              <div class="col-md-6">
                <input id="phone" type="text" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus maxlength="12" minlength="7" placeholder="081xxxxxxxxx">
                @if ($errors->has('phone'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('phone') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group row">
              <label for="value" class="col-sm-4 col-form-label text-md-right">Value</label>
              <div class="col-md-6">
                <select id="value" class="form-control {{ $errors->has('value') ? ' is-invalid' : '' }}" name="value" required>
                  <option value="10000">Rp 10.000</option>
                  <option value="50000">Rp 50.000</option>
                  <option value="100000">Rp 100.000</option>
                </select>
                @if ($errors->has('value'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('value') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <div class="form-group row mb-0">
              <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                  Submit
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection