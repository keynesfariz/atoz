@extends('layouts.app')

@php
  $counter = 1;
  if (isset($_GET['page'])) {
      $counter = $counter + ($_GET['page'] - 1) * 20;
  }
@endphp

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-8">
              My Orders
            </div>
            <div class="col-md-4">
              <form action="{{ url()->current() }}" method="get">
                <div class="input-group pull-right">
                  <input class="form-control" name="order" placeholder="Search order number here..." type="text">
                  <div class="input-group-prepend">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="card-body">
          <table class="table">
            <tr>
              <th>#</th>
              <th>Order No.</th>
              <th>Description</th>
              <th>Total</th>
              <th>Information</th>
            </tr>
            @foreach ($orders as $key => $item)
              <tr>
                <td>{{ $key + $counter }}</td>
                <td>{{ $item->order_number }}</td>

                @if ($item->orderable_type == 'App\PrepaidBalance')
                  <td>{{ number_format($item->orderable->value, 0, ',', '.') }} For {{ $item->orderable->phone }}</td>
                @else
                  <td>{{ $item->orderable->product }} that cost {{ $item->orderable->price }}</td>
                @endif

                <td>Rp {{ number_format($item->total, 0, ',', '.') }}</td>
                
                @php
                  $now = Carbon\Carbon::now();
                  $order_date = $item->created_at;
                  $order_date->addMinutes(5);
                @endphp

                @if ($item->status == 0 && $now->lt($order_date))
                  <td>
                    <a href="{{ url('payment?order='.$item->order_number) }}" class="btn btn-sm btn-primary">Pay</a>
                  </td>
                @elseif ($item->status == 0 && $now->gte($order_date))
                  <td><strong class="text-danger">Failed</strong></td>
                @elseif ($item->orderable_type == 'App\Product')
                  <td><strong>Shipping Code: {{ $item->orderable->shipping_code }}</strong></td>
                @else
                  <td><strong class="text-success">Success</strong></td>
                @endif
              </tr>
            @endforeach
          </table>
          <div class="float-right">
            {{ $orders->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>@endsection