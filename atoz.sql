-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 27. Apr 2018 um 10:56
-- Server-Version: 10.2.12-MariaDB
-- PHP-Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `atoz`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2018_04_27_051133_create_products_table', 1),
(9, '2018_04_27_051231_create_prepaid_balances_table', 1),
(10, '2018_04_27_051403_create_orders_table', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderable_id` int(11) NOT NULL,
  `orderable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `orders`
--

INSERT INTO `orders` (`id`, `order_number`, `orderable_id`, `orderable_type`, `user_id`, `total`, `status`, `created_at`, `updated_at`) VALUES
(1, '1BYPZUrC6t', 1, 'App\\PrepaidBalance', 1, 10500, 0, '2018-04-27 00:07:48', '2018-04-27 00:07:48'),
(2, 'amAF7guC9a', 2, 'App\\PrepaidBalance', 1, 105000, 0, '2018-04-27 00:46:57', '2018-04-27 00:46:57'),
(3, 'vXlwGQWQeq', 3, 'App\\PrepaidBalance', 1, 10500, 0, '2018-04-27 01:01:10', '2018-04-27 01:01:10'),
(4, 'YVbk3j1sMF', 4, 'App\\PrepaidBalance', 1, 10500, 0, '2018-04-27 01:01:26', '2018-04-27 01:01:26'),
(5, 'aludpKbjKz', 5, 'App\\PrepaidBalance', 1, 10500, 0, '2018-04-27 01:01:47', '2018-04-27 01:01:47'),
(6, 'qnmr9QvAuS', 6, 'App\\PrepaidBalance', 1, 10500, 0, '2018-04-27 01:01:59', '2018-04-27 01:01:59'),
(7, 'IeaHRwrVVw', 7, 'App\\PrepaidBalance', 1, 105000, 0, '2018-04-27 02:47:13', '2018-04-27 02:47:13'),
(8, '1xu17LC54e', 8, 'App\\PrepaidBalance', 1, 105000, 0, '2018-04-27 02:50:48', '2018-04-27 02:50:48'),
(9, 'EYhB1iBqmF', 9, 'App\\PrepaidBalance', 1, 105000, 0, '2018-04-27 02:51:07', '2018-04-27 02:51:07'),
(10, 'Sjjjf2y7gn', 10, 'App\\PrepaidBalance', 1, 105000, 0, '2018-04-27 02:51:26', '2018-04-27 02:51:26'),
(11, 'ki5BQ8YBJX', 11, 'App\\PrepaidBalance', 1, 105000, 0, '2018-04-27 02:51:38', '2018-04-27 02:51:38'),
(12, 'c2PCQDQ5wL', 12, 'App\\PrepaidBalance', 1, 105000, 0, '2018-04-27 02:51:53', '2018-04-27 02:51:53'),
(13, 'WQnu7WbH1B', 13, 'App\\PrepaidBalance', 1, 52500, 2, '2018-04-27 03:04:46', '2018-04-27 03:06:23'),
(14, '2726402392', 18, 'App\\PrepaidBalance', 1, 52500, 2, '2018-04-27 03:18:17', '2018-04-27 03:18:21'),
(15, '3327741670', 19, 'App\\PrepaidBalance', 1, 10500, 0, '2018-04-27 03:25:35', '2018-04-27 03:25:35'),
(16, '7734936273', 20, 'App\\PrepaidBalance', 1, 105000, 2, '2018-04-27 03:25:41', '2018-04-27 03:30:01'),
(17, '2399365093', 21, 'App\\PrepaidBalance', 1, 105000, 0, '2018-04-27 03:25:45', '2018-04-27 03:25:45'),
(18, '6169938004', 22, 'App\\PrepaidBalance', 1, 52500, 2, '2018-04-27 03:25:50', '2018-04-27 03:27:32'),
(19, '8326275164', 23, 'App\\PrepaidBalance', 1, 10500, 0, '2018-04-27 03:26:00', '2018-04-27 03:26:00'),
(20, '3052757805', 24, 'App\\PrepaidBalance', 1, 10500, 2, '2018-04-27 03:26:03', '2018-04-27 03:27:35'),
(21, '9748327658', 25, 'App\\PrepaidBalance', 1, 10500, 0, '2018-04-27 03:26:08', '2018-04-27 03:26:08'),
(22, '6340222042', 1, 'App\\Product', 1, 10000, 2, '2018-04-27 03:44:58', '2018-04-27 03:50:07'),
(23, '2659354198', 2, 'App\\Product', 1, 180000, 2, '2018-04-27 03:46:03', '2018-04-27 03:47:37'),
(24, '2117455998', 3, 'App\\Product', 1, 21010000, 2, '2018-04-27 03:51:21', '2018-04-27 03:51:29');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `prepaid_balances`
--

CREATE TABLE `prepaid_balances` (
  `id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `prepaid_balances`
--

INSERT INTO `prepaid_balances` (`id`, `phone`, `value`, `created_at`, `updated_at`) VALUES
(1, '08123121123', 10000, '2018-04-27 00:07:48', '2018-04-27 00:07:48'),
(2, '08184297776', 100000, '2018-04-27 00:46:57', '2018-04-27 00:46:57'),
(3, '08123121123', 10000, '2018-04-27 01:01:10', '2018-04-27 01:01:10'),
(4, '08123121123', 10000, '2018-04-27 01:01:26', '2018-04-27 01:01:26'),
(5, '08123121123', 10000, '2018-04-27 01:01:47', '2018-04-27 01:01:47'),
(6, '08123121123', 10000, '2018-04-27 01:01:59', '2018-04-27 01:01:59'),
(7, '081234567890', 100000, '2018-04-27 02:47:13', '2018-04-27 02:47:13'),
(8, '081234567890', 100000, '2018-04-27 02:50:48', '2018-04-27 02:50:48'),
(9, '081234567890', 100000, '2018-04-27 02:51:07', '2018-04-27 02:51:07'),
(10, '081234567890', 100000, '2018-04-27 02:51:26', '2018-04-27 02:51:26'),
(11, '081234567890', 100000, '2018-04-27 02:51:38', '2018-04-27 02:51:38'),
(12, '081234567890', 100000, '2018-04-27 02:51:53', '2018-04-27 02:51:53'),
(13, '08123121123', 50000, '2018-04-27 03:04:46', '2018-04-27 03:04:46'),
(14, '08184297776', 50000, '2018-04-27 03:16:54', '2018-04-27 03:16:54'),
(15, '08184297776', 50000, '2018-04-27 03:17:36', '2018-04-27 03:17:36'),
(16, '08184297776', 50000, '2018-04-27 03:18:05', '2018-04-27 03:18:05'),
(17, '08184297776', 50000, '2018-04-27 03:18:11', '2018-04-27 03:18:11'),
(18, '08184297776', 50000, '2018-04-27 03:18:17', '2018-04-27 03:18:17'),
(19, '081234567890', 10000, '2018-04-27 03:25:35', '2018-04-27 03:25:35'),
(20, '08123121123', 100000, '2018-04-27 03:25:41', '2018-04-27 03:25:41'),
(21, '08123121123', 100000, '2018-04-27 03:25:45', '2018-04-27 03:25:45'),
(22, '08123121123', 50000, '2018-04-27 03:25:50', '2018-04-27 03:25:50'),
(23, '08123121123', 10000, '2018-04-27 03:26:00', '2018-04-27 03:26:00'),
(24, '081234567890', 10000, '2018-04-27 03:26:03', '2018-04-27 03:26:03'),
(25, '081647324723', 10000, '2018-04-27 03:26:08', '2018-04-27 03:26:08');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `shipping_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'null',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `products`
--

INSERT INTO `products` (`id`, `product`, `address`, `price`, `shipping_code`, `created_at`, `updated_at`) VALUES
(1, 'MakeUp, Hastags, Foods', '411 Elm St.', 200000, 'null', '2018-04-27 03:44:58', '2018-04-27 03:44:58'),
(2, 'Apple Macbook Pro 13\"', '2 Palmerah Utara III St.', 170000, 'null', '2018-04-27 03:46:03', '2018-04-27 03:46:03'),
(3, 'Apple iPhone X 256GB Jet Black', '2 Palmerah Utara III St.', 21000000, '4PWSE40JZP', '2018-04-27 03:51:21', '2018-04-27 03:51:29');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Muhammad Fariz', 'muhafariz@gmail.com', '$2y$10$lRafxZt4PzRPZ6G4.SM3IukWl7q9T6JlWI1pyWK01IH4.RRt7Kopy', 'Iypm8nqBibFjx1LtG097Bzq3sL2ze2KIML0eVyWI4rGSJQQMVvWKBxQZZ1Zr', '2018-04-27 00:06:56', '2018-04-27 00:06:56');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indizes für die Tabelle `prepaid_balances`
--
ALTER TABLE `prepaid_balances`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT für Tabelle `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT für Tabelle `prepaid_balances`
--
ALTER TABLE `prepaid_balances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT für Tabelle `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
